using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Textos : MonoBehaviour
{

    //Variables
    private Text Bronce;
    private Text Oro;
    private Text Diamante;
    public Text Puntos;
    public Text Vida;

    //Acomular
    private int PunBro = 0;
    private int PunOro = 0;
    private int PuntDia = 0;
    private int PunTotal = 0;
    private int PunVida = 5;

    void Start()
    {
        Vida.text = "Vidas : " + PunVida;
        Puntos.text = "Puntos : " + PunTotal;
    }

    //Puntaje

    public void SumPuntos(int bro, int or, int di)
    {
        this.PunBro += bro;
        this.PunOro += or;
        this.PuntDia += di;
        this.PunTotal += (bro + or + di);
        Puntos.text = "Puntos : " + PunTotal;
    }

    public void Menosvidas(int vidas)
    {
        this.PunVida -= vidas;
        Vida.text = "Vidas : " + PunVida;
    }

}
