using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienController : MonoBehaviour
{
    public int vidas = 3;
    public float velocityX = -5f;
    
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    private BoxCollider2D box;

    public int ANIMATION_DEAD= 3;
    public float TIME_DEAD = 0f;
    public bool DEAD = false;


    //Dato
    public static int dato;
    public int refeDato = 5;

    void Start()
    {
        dato = refeDato;

        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        box = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
        if(DEAD){
            TIME_DEAD += Time.deltaTime;
            if(TIME_DEAD>=1.7){
                Destroy(this.gameObject);
            }
        }
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Shoot"))
        {
            Destroy(other.gameObject);
            vidas -=1;
            if(vidas <= 0){
                dead();
            }
            
        }
        if (other.gameObject.CompareTag("Shoot1"))
        {
            Destroy(other.gameObject);
            vidas -=2;
            if(vidas <= 0){
                dead();
            }
            
        }
        if (other.gameObject.CompareTag("Shoot2"))
        {
            Destroy(other.gameObject);
            vidas -=3;
            if(vidas <= 0){
                dead();
            }
            
        }
        if (other.gameObject.CompareTag("pared"))
        {
            sr.flipX = !sr.flipX;
            
            velocityX = velocityX * -1;
        }
        
    }

    private void changeAnimation(int animation)
    {
        animator.SetInteger("Estado", animation);
    }
    private void dead()
    {
        changeAnimation(ANIMATION_DEAD);
        DEAD=true;
        velocityX=0;
        box.enabled=false;
    }
}
