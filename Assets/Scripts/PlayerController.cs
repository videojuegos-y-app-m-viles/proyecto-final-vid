using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
        // public properties
    public float velocityX = 15;
    public float jumpForce = 1500;
    public float charge = 0;

    public float TIME_DEAD = 0f;
    public bool DEAD = false;

    public GameObject rightBullet;
    public GameObject leftBullet;

    public GameObject rightBullet1;
    public GameObject leftBullet1;

    public GameObject rightBullet2;
    public GameObject leftBullet2;

    // private components
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    
    
    // private properties
    private bool isIntangible = false;
    private float intangibleTime = 0f;
    private bool jump = false;
    
    // constants
    private const int ANIMATION_IDLE = 0;
    private const int ANIMATION_RUN = 1;
    private const int ANIMATION_SLIDE= 2;
    private const int ANIMATION_JUMP= 3;
    private const int ANIMATION_SHOOT= 4;
    private const int ANIMATION_DEAD= 5;
    private const int ANIMATION_RUNSHOOT= 6;
    private const int ANIMATION_JUMPSHOOT= 7;
    private const int ANIMATION_JUMPMELEE= 8;

    //Puntajes
    private Textos Scor;

    //Vida
    public int vidas = 10;


    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        Scor = FindObjectOfType<Textos>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        if(DEAD){
            TIME_DEAD += Time.deltaTime;
            if(TIME_DEAD>=0.8){
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else{
            changeAnimation(ANIMATION_IDLE);
        
            if (Input.GetKey(KeyCode.RightArrow))//drecha
            {
                rb.velocity = new Vector2(velocityX, rb.velocity.y); 
                sr.flipX = false;
                changeAnimation(ANIMATION_RUN);
            }
            
            if (Input.GetKey(KeyCode.LeftArrow))//izquierda
            {
                rb.velocity = new Vector2(-velocityX, rb.velocity.y);
                sr.flipX = true;
                changeAnimation(ANIMATION_RUN);
            }

            if (Input.GetKey(KeyCode.C)){
                charge += Time.deltaTime;
                
                if (sr.color == Color.red)
                {
                    sr.color= Color.white;
                }else{
                    sr.color= Color.red;
                }
            }

            if (Input.GetKeyUp(KeyCode.C))
            {
                changeAnimation(ANIMATION_SHOOT);
                sr.color= Color.white;
                if (charge >= 5)
                {
                    var bullet = sr.flipX ? leftBullet2 : rightBullet2;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet2.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    if (charge >= 3)
                    {
                        var bullet = sr.flipX ? leftBullet1 : rightBullet1;
                        var position = new Vector2(transform.position.x, transform.position.y);
                        var rotation = rightBullet1.transform.rotation;
                        Instantiate(bullet, position, rotation);
                    }else
                    {
                        var bullet = sr.flipX ? leftBullet : rightBullet;
                        var position = new Vector2(transform.position.x, transform.position.y);
                        var rotation = rightBullet.transform.rotation;
                        Instantiate(bullet, position, rotation);
                    }
                }
                charge=0;
            }

            
            
            if (Input.GetKey(KeyCode.X))//desizar
            {
                changeAnimation(ANIMATION_SLIDE);
            }

            if (Input.GetKeyUp(KeyCode.Space))//saltar
            {
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // salta
                changeAnimation(ANIMATION_JUMP); // saltar
            }
        }
        
        
    }

//Colisiones

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Puntajes
        if (collision.gameObject.CompareTag("Bronze"))
        {
            Destroy(collision.gameObject);
            Scor.SumPuntos(1, 0, 0);
        }
        if (collision.gameObject.CompareTag("Gold"))
        {
            Destroy(collision.gameObject);
            Scor.SumPuntos(0, 3, 0);
        }
        if (collision.gameObject.CompareTag("Silver"))
        {
            Destroy(collision.gameObject);
            Scor.SumPuntos(0, 0, 5);
        }

        if (collision.gameObject.CompareTag("Hazsherk") || collision.gameObject.CompareTag("Groven"))
        {
            vidas -= AlienController.dato;
            Debug.Log("Enemigo me cuaso da�o");
            if (vidas == 0)
            {
                Scor.Menosvidas(1);
                Debug.Log("He muerto");
                dead();
                vidas = 10;
            }
            
        }
        if (collision.gameObject.CompareTag("Muerte"))
        {
            Scor.Menosvidas(1);
            Debug.Log("He muerto");
            dead();
            vidas = 10;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (collision.gameObject.CompareTag("Cambio"))
        {
            //SE CAMBIA LA ECENA
            Debug.Log("Cambuio de Ecena");
            SceneManager.LoadScene("Ecena2");
        }
        if (collision.gameObject.CompareTag("Final"))
        {
            //SE CAMBIA LA ECENA
            Debug.Log("Cambuio de Ecena");
            SceneManager.LoadScene("Final");
        }

    }
    private void changeAnimation(int animation)
    {
        animator.SetInteger("Estado", animation);
    }
    private void dead()
    {
        changeAnimation(ANIMATION_DEAD);
        DEAD=true;
        velocityX=0;
    }
}
